package com.example.nj.blogeditor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by nj on 1/4/18.
 */

public class EditView extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_window);

        String story = getIntent().getStringExtra("Story");
        String title = getIntent().getStringExtra("Title");
        String genre = getIntent().getStringExtra("Genre");


        TextView prevStory = (TextView)findViewById(R.id.prevStory);
        final EditText newStory = (EditText)findViewById(R.id.addStory);


        prevStory.setText(story);

        newStory.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if(WordLimitEdit(newStory.getText().toString())){
                    newStory.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(newStory.getText().toString().length()) });
                }

                return false;
            }

        });

    }


    public boolean WordLimitEdit(String s){

        String arr[]=s.split(" ");
        if(arr.length>=50){

            return false;
        }else{

            return true;
        }


    }

}
