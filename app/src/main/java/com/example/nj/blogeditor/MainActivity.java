package com.example.nj.blogeditor;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private LinearLayout mLinearScroll;
    private GridView mListView;
    private ArrayList<Story_Element> mArrayList;
    private ArrayList<Story_Element> mArrayListTemp;
    private StoryAdapter adapter;
    // change row size according to your need, how many row you needed per page
    int rowSize = 10;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        
       
            mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);
            mListView = (GridView) findViewById(R.id.view_grid);

            /**
             * add item into arraylist
             */
            mArrayListTemp = new ArrayList<Story_Element>();

            mArrayList = new ArrayList<Story_Element>();


            mArrayList.add(new Story_Element("Title","Romance","yyy","URL","New Story"));

            populateView(mArrayList);

        final FloatingActionButton searchh=(FloatingActionButton)findViewById(R.id.searchh);
        FloatingActionButton sortt=(FloatingActionButton)findViewById(R.id.sort);

        searchh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        sortt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SortStories();
            }
        });


        }


        public void populateView(final ArrayList<Story_Element> mArrayList){
            /**
             * create dynamic button according the size of array
             */



            int rem = mArrayList.size() % rowSize;
            if (rem > 0) {

                for (int i = 0; i < rowSize - rem; i++) {
                    mArrayList.add(new Story_Element("XXX","XXX","xxx","XXX","xxx"));

                }
            }

            /**
             * add arraylist item into list on page load
             */
            addItem(0,mArrayList);

            int size = mArrayList.size() / rowSize;

            for (int j = 0; j < size; j++) {
                final int k;
                k = j;
                final Button btnPage = new Button(MainActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(5, 2, 2, 2);
                btnPage.setTextColor(Color.WHITE);
                btnPage.setTextSize(13.0f);
                btnPage.setId(j);
                btnPage.setText(String.valueOf(j + 1));
                mLinearScroll.addView(btnPage, lp);

                btnPage.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        /**
                         * add arraylist item into list
                         */
                        addItem(k,mArrayList);

                    }
                });
            }

        }

    //create dynamic temp array list from main-list
    public void addItem(int count,ArrayList<Story_Element> mArrayList) {
        mArrayListTemp.clear();
        count = count * rowSize;
        /**
         * fill temp array list to set on page change
         */
        for (int j = 0; j < rowSize; j++) {
            mArrayListTemp.add(j, mArrayList.get(count));
            count = count + 1;
        }
        // set view
        setView();
    }


    //set view method
    public void setView() {

        adapter = new StoryAdapter(this, R.layout.dashboard_story_element,mArrayListTemp);
        mListView.setAdapter(adapter);


    }

    public void SortStories(){



//        ArrayList<Story_Element> arr = mArrayList;
//
        Collections.sort(mArrayListTemp, new Comparator<Story_Element>() {
            @Override
            public int compare(Story_Element story_element, Story_Element t1) {

                if(story_element.publishDate.compareTo(t1.publishDate)>0){
                    return 1;

                }else if(story_element.publishDate.compareTo(t1.publishDate)<0){
                    return -1;
                }
                return 0;
            }
        });
        adapter.notifyDataSetChanged();

//
//        populateView(arr);
    }


    public void search(){
        ArrayList<Story_Element> arr = mArrayList;

        String ar[]=new String[arr.size()];

        for(int i=0;i<arr.size();i++){

            ar[i]=arr.get(i).title;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item,ar );
        //Getting the instance of AutoCompleteTextView
        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.search);
        actv.setThreshold(1);//will start working from first character
        actv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        actv.setTextColor(Color.BLUE);

    }



}
