package com.example.nj.blogeditor;

import android.media.Image;

import java.util.Date;

/**
 * Created by nj on 1/4/18.
 */

public class Story_Element {

    public String title="xxx";
    public String genre="xxx";
    public String publishDate="xxx";
    public String ImageUrl="xxx";
    public String story="xxx";
    public String status="xxx";

    public Story_Element(String title,String genre,String publishDate,String ImageUrl,String story){

        this.title=title;
        this.genre=genre;
        this.publishDate=publishDate;
        this.ImageUrl= ImageUrl;
        this.story=story;

        this.status=WordLimit(story);

    }


    public String WordLimit(String s){

        String arr[]=s.split(" ");
        if(arr.length>=500){

            return "Completed";
        }else{

            return "Running";
        }


    }

}
