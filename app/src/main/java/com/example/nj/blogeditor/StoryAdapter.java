package com.example.nj.blogeditor;

/**
 * Created by nj on 1/4/18.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class StoryAdapter extends ArrayAdapter {
    ArrayList<Story_Element> msg_list = new ArrayList<>();
    Context con;

    public StoryAdapter(Context context, int resource, ArrayList<Story_Element> msg_list) {
        super(context, resource);
        this.con = context;
        this.msg_list = msg_list;
    }

    @Override
    public int getCount() {
        return msg_list.size();
    }

    @Override
    public Story_Element getItem(int position) {
        return msg_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.dashboard_story_element, parent, false);

        }
        TextView title = (TextView) row.findViewById(R.id.story_name);
        TextView status = (TextView) row.findViewById(R.id.story_status);
        TextView genre = (TextView) row.findViewById(R.id.story_genre);
        ImageView imge = (ImageView) row.findViewById(R.id.cover);
        LinearLayout lay1=(LinearLayout)row.findViewById(R.id.layy1);

        final Story_Element item = msg_list.get(position);

        title.setText(item.title);
        genre.setText(item.genre);

        //status.setText("Author: " + item.author);
        new DownloadImageTask(imge)
                .execute(item.ImageUrl);

        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EditView.class);

                i.putExtra("Title", item.title);
                i.putExtra("Genre", item.genre);
                i.putExtra("Story", item.story);

                getContext().startActivity(i);

            }
        });

        //row.setLayoutParams(new GridLayout.LayoutParams());
        return row;
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
           // Drawable d = new BitmapDrawable(result);
            bmImage.setImageBitmap(result);
        }
    }
}